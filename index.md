# Egon Kidmose - Post Doc interested in network security, open software and much more #

Primary purpose of this page is to host stuff others might find usefull, see [Publications](#publications).

## Activities ##
Check out [@ekidmose](https://twitter.com/ekidmose) on twitter to see what I'm currently digging into, [github](https://github.com/kidmose) to see if I'm working on anything public and [LinkedIn](https://dk.linkedin.com/in/kidmose) for a reasonably up to date Curicula Vitae.

**(meaning, don't expect frequent updates here)**

## Identity ##
HTTPS is currently broken on this server, without expectations of a fix, so please check out [keybase.io](https://keybase.io/kidmose) for public key etc.

## Publications ##
<a name="publications">

I have contributed to following peer-reviewed publications:

 * Kidmose, Egon, Matija Stevanovic, and Jens Myrup Pedersen. "Detection of malicious domains through lexical analysis." In 2018 International Conference on Cyber Security and Protection of Digital Services (Cyber Security), pp. 1-5. IEEE, 2018. [Download](./papers/kidmose_2018_detection_of_malicious_domains_through_lexical_analysis.pdf)
 * Kidmose, Egon, Erwin Lansing, Søren Brandbyge, and Jens Myrup Pedersen. "Detection of malicious and abusive domain names." In 2018 1st International Conference on Data Intelligence and Security (ICDIS), pp. 49-56. IEEE, 2018. [Download](./papers/kidmose_2018_detection_of_malicious_and_abusive_domain_names.pdf)
 * Pedersen, Jens Myrup, and Egon Kidmose. "Security in Internet of Things: Trends and Challenges." In BIR Workshops, pp. 182-188. 2018. [Download](./papers/pedersen_2018_security_in_internet_of_things_trends_and_challenges.pdf)
 * Kidmose, Egon, Kristian Gausel, Søren Brandbyge, and Jens Myrup Pedersen. "Assessing Usefulness of Blacklists Without the Ground Truth." In International Conference on Image Processing and Communications, pp. 216-223. Springer, Cham, 2018. [Download](./papers/kidmose_2018_assessing_usefulness_of_blacklists_without_the_ground_truth.pdf)
 * Kidmose, Egon, Erwin Lansing, Søren Brandbyge, and Jens Myrup Pedersen. "Heuristic Methods for Efficient Identification of Abusive Domain Names." IJCSA 3, no. 1 (2018): 121-142. [Download](./papers/kidmose_2018_heuristic_methods_for_efficient_identification_of_abusive_domain_names.pdf)
 * Shahid, Kamal, Egon Kidmose, Rasmus L. Olsen, Lennart Petersen, and Florin Iov. "On the impact of cyberattacks on voltage control coordination by ReGen plants in smart grids." In 2017 IEEE International Conference on Smart Grid Communications (SmartGridComm), pp. 480-485. IEEE, 2017. [Download](./papers/shahid_2017_on_the_impact_of_cyberattacks_on_voltage_control_coordination_by_regen_plants_in_smart_grids.pdf)
 * Kidmose, Egon, and Jens Myrup Pedersen. "Security in Internet of Things." Cybersecurity and Privacy: Bridging the Gap. River Publishers (2017): 99-118. [Download](./papers/kidmose_2017_security_in_internet_of_things.pdf)
 * Kidmose, Egon, Matija Stevanovic, and Jens Myrup Pedersen. "Correlating intrusion detection alerts on bot malware infections using neural network." In 2016 International Conference On Cyber Security And Protection Of Digital Services (Cyber Security), pp. 1-8. IEEE, 2016. [Download](./papers/kidmose_2016_correlating_intrusion_detection_alerts_on_bot_malware_infections_using_neural_network.pdf)
 * Kidmose, Egon, Emad Ebeid, and Rune Hylsberg Jacobsen. "A Framework for Detecting and Translating User Behavior from Smart Meter Data." In Fourth International Conference on Smart Systems, Devices and Technologies, IARIA, 2015). [Download](./papers/kidmose_2015_a_framework_for_detecting_and_translating_user_behavior_from_smart_meter_data.pdf)

My master and PhD thesises:

 * Kidmose, Egon. "Network-based detection of malicious activities-a corporate network perspective." PhD diss., Aalborg Universitetsforlag, 2019. [Download](./papers/kidmose_2019_network-based_detection_of_malicious_activities_-_a_corporate_network_perspective.pdf)
 * Botnet detection using Hidden Markov Models, Egon Kidmose, Master Thesis at Aalborg University, 2014. [Download](./paper/kidmose_2014_botnet_detection_using_hidden_markov_models.pdf)

I have contributed to the following papers, which have not been published through peer-reviewed channels:

 * Hedayati, Fares, and Egon Kidmose. "Ensemble Bayes Tree." (2013). [Download](./papers/hedayati_2013_ensemble_bayes_tree.pdf)

My appearances in the media include:

 * A radio interview for DR P4 Nordjylland, a local program of the Danish national radio. [Download](./media/2019-06-19-17_15_00_p4_nordjylland_-_p4_eftermiddag_-_egon_kidmose.mp3)

